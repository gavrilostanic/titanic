import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    
    df['Name'] = df['Name'].str.extract(r'\w+\s+(\w+)')
    missing_ages = df.groupby('Name')['Age'].apply(lambda x: x.isna().sum())
    median_ages = df.groupby('Name')['Age'].median()

    result = [('Mr.',missing_ages['Mr.'],median_ages['Mr.'].round()), 
            ('Mrs.',missing_ages['Mrs.'],median_ages['Mrs.'].round()),
            ('Miss.',missing_ages['Miss.'],median_ages['Miss.'].round())]
    return result
